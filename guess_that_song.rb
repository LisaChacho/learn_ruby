# Guess what song this is! :-)

# Run this file using the following command:
# $ ruby guess_that_song.rb

# Debug this file using the following command:
# $ ruby -rdebug guess_that_song.rb
# And navigate using:
# b <line>      put break point
# n             next
# s             step
# c             continue
# p             puts

# Or if you're a big prybaby (like me)...
# Add `require 'pry'` to top-level dependencies
# Insert inline `binding.pry` and navigate using:
# next/step/continue/finish

class Song
    require 'date'
    attr_accessor :me

    def tell_me
        me = { dance: ['mash_potato', 'twist'], now: DateTime.now }

        if love?(me)
            watch me[:now]
        end
    end

    def love?(me)
        can_dance?(me[:dance])
    end

    def can_dance?(like_this)
        like_this.any?
    end

    def watch(now)
        puts now.strftime("%m/%d/%Y %I:%M:%S %p")
        2.times { puts 'work' }
    end

    Song.new.tell_me
end
